const webpack = require('webpack');
const path = require('path');
const HtmlWebpackPlugin = require('html-webpack-plugin');

const SRC_PATH = path.resolve(__dirname, '../src');

module.exports = {
  resolve: {
    alias: {
      '@src': SRC_PATH,
      '@components': `${SRC_PATH}/components`,
      '@pages': `${SRC_PATH}/pages`,
      '@layouts': `${SRC_PATH}/layouts`,
      '@redux': `${SRC_PATH}/redux`,
      '@hooks': `${SRC_PATH}/hooks`,
      'react-dom': '@hot-loader/react-dom',
    },
    extensions: ['.js', '.json', '.jsx'],
  },
  context: path.join(__dirname, '../'),
  devtool: 'inline-source-map',
  entry: [
    'babel-polyfill',
    'react-hot-loader/patch',
    'webpack-dev-server/client?http://localhost:8080',
    'webpack/hot/only-dev-server',
    './src/config.js',
    './src/index.js',
    './src/styles/main.scss'
  ],
  mode: 'development',
  output: {
    path: path.join(__dirname, '../build'),
    filename: './js/index.js',
    publicPath: '/'
  },
  devServer: {
    hot: true,
    publicPath: '/',
    historyApiFallback: true,
    open: true,
    host: '0.0.0.0'
  },
  watchOptions: {
    aggregateTimeout: 300,
    poll: 1000,
  },
  module: {
    rules: [
      {
        test: /\.(js|jsx)$/,
        exclude: /node_modules/,
        use: {
          loader: 'babel-loader',
          options: {
            presets: ['@babel/env', '@babel/react'],
            plugins: [
              [
                '@babel/plugin-proposal-decorators',
                { legacy: true }
              ],
              '@babel/plugin-proposal-class-properties'
            ]
          }
        }
      },
      {
        test: /\.(scss|css)$/,
        use: ['style-loader', 'css-loader', 'sass-loader']
      },
      {
        test: /\.(pdf)$/,
        use: [
          {
            loader: 'file-loader',
            options: {
              name: '[name]-[hash].[ext]'
            }
          }
        ]
      },
      {
        test: /\.(woff|woff2|ttf|eot|svg)$/,
        use: [
          {
            loader: 'file-loader'
          }
        ]
      },
      {
        test: /\.(jpg|png)$/,
        use: {
          loader: 'url-loader',
        },
      },
    ]
  },
  plugins: [
    new webpack.ContextReplacementPlugin(/moment[\/\\]locale$/, /pl/),
    new webpack.ProvidePlugin({
      fetch:
        'imports-loader?this=>global!exports-loader?global.fetch!whatwg-fetch'
    }),
    new webpack.DefinePlugin({
      'process.env': {
        NODE_ENV: JSON.stringify('development'),
      }
    }),
    new webpack.HotModuleReplacementPlugin(),
    new HtmlWebpackPlugin({
      filename: 'index.html',
      template: path.join(__dirname, '../src/index.html'),
    }),
  ],
  optimization: {
    runtimeChunk: true,
    splitChunks: { chunks: 'all' }
  },
};
