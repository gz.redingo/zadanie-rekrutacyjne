import { combineReducers } from 'redux';
import { characterList } from '@components/characterList';
import { singleCharacter } from '@components/singleCharacter';

export const rootReducer = combineReducers({
  characterList,
  singleCharacter,
});
