import { createLogger } from 'redux-logger';
import promiseMiddleware from 'redux-promise-middleware';

export const middlewares = [
  promiseMiddleware,
];

middlewares.push(createLogger());
