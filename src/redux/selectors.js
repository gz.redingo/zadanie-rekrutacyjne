export const characterListSelector = (state) => state.characterList;
export const singleCharacterSelector = (state) => state.singleCharacter;
