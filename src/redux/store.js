import { applyMiddleware, createStore, compose } from 'redux';

import { middlewares } from '@src/redux/middlewares';
import { rootReducer } from '@src/redux/root-reducer';

// eslint-disable-next-line no-underscore-dangle
const composeEnhancers = window.__REDUX_DEVTOOLS_EXTENSION_COMPOSE__ || compose;

const initialState = {};
const store = createStore(
  rootReducer,
  initialState,
  composeEnhancers(applyMiddleware(...middlewares)),
);

export default store;
