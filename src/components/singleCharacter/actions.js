import { createAction } from 'redux-actions';

import ACTION from '@src/redux/action-types';
import { api } from '@src/repository/api';

export const getSingleCharacter = createAction(ACTION.CHARACTER.SINGLE, (id) => api({
  data: {
    query: `query character {
      character(id: ${ id }) {
        id,
        name,
        image,
        gender,
        location {
          name,
          type,
          dimension,
          created
        },
        status,
        episode {
          name,
          episode
        }
      }
    }`,
  },
}));

export const getSingleCharacterEnd = createAction(ACTION.CHARACTER.SINGLE_END);
