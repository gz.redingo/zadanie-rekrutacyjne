import React, { useEffect } from 'react';
import { useParams, useHistory } from 'react-router-dom';
import { useDispatch, useSelector } from 'react-redux';
import Grid from '@material-ui/core/Grid';
import Typography from '@material-ui/core/Typography';
import Card from '@material-ui/core/Card';
import CardContent from '@material-ui/core/CardContent';
import ListSubheader from '@material-ui/core/ListSubheader';
import List from '@material-ui/core/List';
import ListItem from '@material-ui/core/ListItem';
import ListItemText from '@material-ui/core/ListItemText';
import Button from '@material-ui/core/Button';
import ArrowBackIosIcon from '@material-ui/icons/ArrowBackIos';
import { singleCharacterSelector } from '@redux/selectors';
import { Loader } from '@components/shared';
import { EpisodeList } from './components/EpisodeList';
import { getSingleCharacter } from './actions';

export function SingleCharacter() {
  const { id } = useParams();
  const dispatch = useDispatch();
  const history = useHistory();
  const {
    singleCharacter,
    singleCharacterRequest,
  } = useSelector(singleCharacterSelector);

  useEffect(() => {
    dispatch(getSingleCharacter(id));
  }, [dispatch, id]);

  if (singleCharacterRequest || !singleCharacter) {
    return <Loader />;
  }
  return (
    <Grid container className='character'>
      <Button
        variant='contained'
        color='secondary'
        startIcon={<ArrowBackIosIcon />}
        className='back-btn'
        onClick={() => history.push('/')}
      >
        Back to list
      </Button>
      <Grid item container>
        <Grid item xs={12}>
          <Card raised className='character__card'>
            <CardContent>
              <Grid container>
                <Grid item xs={12}>
                  <Typography gutterBottom variant='h2' component='h2'>
                    {singleCharacter.name}
                  </Typography>
                </Grid>
                <Grid item xs={12} md={4}>
                  <div className='character__image'>
                    <img src={singleCharacter.image} alt={singleCharacter.name} />
                  </div>
                </Grid>
                <Grid item container xs={12} md={8}>
                  <Grid item xs={12} md={4}>
                    {singleCharacter.location && (
                      <List>
                        <ListSubheader disableGutters>
                          Location:
                        </ListSubheader>
                        <ListItem disableGutters>
                          <ListItemText
                            primary={singleCharacter.location.name}
                            secondary={singleCharacter.location.type}
                          />
                        </ListItem>
                      </List>
                    )}
                  </Grid>
                  <Grid item xs={12} md={4}>
                    <List>
                      <ListSubheader disableGutters>
                      Gender:
                      </ListSubheader>
                      <ListItem disableGutters>
                        <ListItemText
                          primary={singleCharacter.gender}
                        />
                      </ListItem>
                    </List>
                  </Grid>
                  <Grid item xs={12} md={4}>
                    <List>
                      <ListSubheader disableGutters>
                      Status:
                      </ListSubheader>
                      <ListItem disableGutters>
                        <ListItemText
                          primary={singleCharacter.status}
                        />
                      </ListItem>
                    </List>
                  </Grid>
                  <Grid item xs={12}>
                    {singleCharacter.episode && (
                      <>
                        <ListSubheader disableGutters>
                          Episodes:
                        </ListSubheader>
                        <EpisodeList episodeList={singleCharacter.episode} />
                      </>
                    )}
                  </Grid>
                </Grid>
              </Grid>
            </CardContent>
          </Card>
        </Grid>
      </Grid>
    </Grid>
  );
}
