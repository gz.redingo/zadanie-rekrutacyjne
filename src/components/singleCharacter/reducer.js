import { handleActions } from 'redux-actions';

import ACTION from '@src/redux/action-types';

const INITIAL_STATE = {
  singleCharacter: false,
  singleCharacterRequest: false,
  singleCharacterFailure: false,
};

export const reducer = handleActions({
  [ACTION.CHARACTER.SINGLE_PENDING]: (state) => ({
    ...state,

    singleCharacter: false,
    singleCharacterRequest: true,
    singleCharacterFailure: false,
  }),

  [ACTION.CHARACTER.SINGLE_FULFILLED]: (state, action) => ({
    ...state,

    singleCharacter: action.payload.data.data.character,
    singleCharacterRequest: false,
    singleCharacterFailure: false,
  }),

  [ACTION.CHARACTER.SINGLE_REJECTED]: (state, action) => ({
    ...state,

    singleCharacter: false,
    singleCharacterRequest: false,
    singleCharacterFailure: action.payload,
  }),

  [ACTION.CHARACTER.SINGLE_END]: () => INITIAL_STATE,

}, INITIAL_STATE);
