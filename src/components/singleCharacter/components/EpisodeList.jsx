import React, { useState, useEffect } from 'react';
import PropTypes from 'prop-types';
import Paper from '@material-ui/core/Paper';
import Table from '@material-ui/core/Table';
import TableHead from '@material-ui/core/TableHead';
import TableBody from '@material-ui/core/TableBody';
import TableCell from '@material-ui/core/TableCell';
import TableContainer from '@material-ui/core/TableContainer';
import TableFooter from '@material-ui/core/TableFooter';
import TablePagination from '@material-ui/core/TablePagination';
import TableRow from '@material-ui/core/TableRow';
import map from 'lodash/map';
import slice from 'lodash/slice';

export function EpisodeList(props) {
  const { episodeList } = props;
  const [page, setPage] = useState(0);
  const [data, setData] = useState([]);

  useEffect(() => {
    setData(slice(episodeList, 0, 5));
  }, [episodeList]);

  useEffect(() => {
    setData(slice(episodeList, page * 5, (page * 5) + 5));
  }, [episodeList, page]);

  const handleChangePage = (e, newPage) => {
    setPage(newPage);
  };

  return (
    <TableContainer component={Paper}>
      <Table>
        <TableHead>
          <TableRow>
            <TableCell align='left'><b>Name</b></TableCell>
            <TableCell align='right'><b>Episopde</b></TableCell>
          </TableRow>
        </TableHead>
        <TableBody>
          {map(data, (episode) => (
            <TableRow key={episode.name}>
              <TableCell align='left'>
                {episode.name}
              </TableCell>
              <TableCell align='right'>
                {episode.episode}
              </TableCell>
            </TableRow>
          ))}
        </TableBody>
        <TableFooter>
          <TableRow>
            <TablePagination
              rowsPerPageOptions={[]}
              colSpan={3}
              count={episodeList.length}
              rowsPerPage={5}
              page={page}
              SelectProps={{
                inputProps: { 'aria-label': 'rows per page' },
                native: true,
              }}
              onChangePage={handleChangePage}
            />
          </TableRow>
        </TableFooter>
      </Table>
    </TableContainer>
  );
}

EpisodeList.propTypes = {
  episodeList: PropTypes.array.isRequired,
};
