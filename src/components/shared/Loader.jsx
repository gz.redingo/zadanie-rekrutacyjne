import React from 'react';
import CircularProgress from '@material-ui/core/CircularProgress';
import Grid from '@material-ui/core/Grid';

export function Loader() {
  return (
    <Grid
      container
      direction='row'
      justify='center'
      alignItems='center'
      className='loader'
    >
      <CircularProgress
        color='secondary'
      />
    </Grid>
  );
}
