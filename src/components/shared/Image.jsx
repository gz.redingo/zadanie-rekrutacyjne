import React, { useState } from 'react';
import PropTypes from 'prop-types';
import { Loader } from '@components/shared';

export function Image(props) {
  const [loaded, setLoaded] = useState('');
  const { src, alt } = props;

  return (
    <>
      {!loaded && (
        <Loader />
      )}
      <img className={`custom-image ${ loaded }`} src={src} alt={alt} onLoad={() => setLoaded('loaded')} />
    </>
  );
}

Image.propTypes = {
  alt: PropTypes.string.isRequired,
  src: PropTypes.string.isRequired,
};
