import { handleActions } from 'redux-actions';

import ACTION from '@src/redux/action-types';

const INITIAL_STATE = {
  characterList: false,
  characterListRequest: false,
  characterListFailure: false,
};

export const reducer = handleActions({
  [ACTION.CHARACTER.LIST_PENDING]: (state) => ({
    ...state,

    characterList: false,
    characterListRequest: true,
    characterListFailure: false,
  }),

  [ACTION.CHARACTER.LIST_FULFILLED]: (state, action) => ({
    ...state,

    characterList: action.payload.data,
    characterListRequest: false,
    characterListFailure: false,
  }),

  [ACTION.CHARACTER.LIST_REJECTED]: (state, action) => ({
    ...state,

    characterList: false,
    characterListRequest: false,
    characterListFailure: action.payload,
  }),

  [ACTION.CHARACTER.LIST_END]: () => INITIAL_STATE,

}, INITIAL_STATE);
