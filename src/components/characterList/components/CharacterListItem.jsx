import React from 'react';
import PropTypes from 'prop-types';
import { useHistory } from 'react-router-dom';
import Card from '@material-ui/core/Card';
import CardActionArea from '@material-ui/core/CardActionArea';
import CardContent from '@material-ui/core/CardContent';
import Typography from '@material-ui/core/Typography';
import { Image } from '@components/shared';
// eslint-disable-next-line import/extensions
import placeholder from '@src/assets/images/person.png';

export function CharacterListItem(props) {
  const {
    id,
    image,
    name,
    species,
  } = props;

  const history = useHistory();

  return (
    <Card
      onClick={() => history.push(`/character/${ id }`)}
      className='list__tile'
    >
      <CardActionArea className='list__tile__button'>
        <Image src={image || placeholder} alt={name} />
        <CardContent className='list__tile__content'>
          <Typography gutterBottom variant='h5' component='h2'>
            {name}
          </Typography>
          <Typography variant='body2' color='textSecondary' component='p'>
            Species:&nbsp;
            {species}
          </Typography>
        </CardContent>
      </CardActionArea>
    </Card>
  );
}

CharacterListItem.propTypes = {
  id: PropTypes.string.isRequired,
  image: PropTypes.string.isRequired,
  name: PropTypes.string.isRequired,
  species: PropTypes.string.isRequired,
};
