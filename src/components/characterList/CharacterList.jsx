import React, { useEffect, useState } from 'react';
import { useDispatch, useSelector } from 'react-redux';
import { characterListSelector } from '@redux/selectors';
import Grid from '@material-ui/core/Grid';
import TextField from '@material-ui/core/TextField';
import SearchIcon from '@material-ui/icons/Search';
import Pagination from '@material-ui/lab/Pagination';
import Alert from '@material-ui/lab/Alert';
import { Loader } from '@components/shared';
import map from 'lodash/map';
import { CharacterListItem } from './components/CharacterListItem';
import { getCharacterList } from './actions';

export function CharacterList() {
  const [text, setText] = useState('');
  const [page, setPage] = useState(1);
  const dispatch = useDispatch();
  const {
    characterList,
    characterListRequest,
  } = useSelector(characterListSelector);

  useEffect(() => {
    dispatch(getCharacterList());
  }, [dispatch]);

  const handleSubmit = (e) => {
    e.preventDefault();
    dispatch(getCharacterList(text, 1));
    setPage(1);
  };

  const handleChangePage = (e, nextPage) => {
    dispatch(getCharacterList(text, nextPage));
    setPage(nextPage);
  };

  return (
    <Grid container>
      <Grid item xs={12} className='search'>
        <form onSubmit={handleSubmit}>
          <TextField
            label='Search'
            InputProps={{
              endAdornment: <SearchIcon />,
            }}
            value={text}
            onChange={(e) => setText(e.target.value)}
          />
        </form>
      </Grid>
      {(characterListRequest !== false || characterList === false) ? (
        <Loader />
      ) : (
        <>
          {characterList.errors ? (
            <Grid item xs={12}>
              {map(characterList.errors, (error, index) => (
                <Alert key={index.toString()} severity='error'>{ error.message }</Alert>
              ))}
            </Grid>
          ) : (
            <>
              <Grid container spacing={3} className='list'>
                {map(characterList.data.characters.results, (character, index) => (
                  <Grid
                    item
                    xs={12}
                    sm={6}
                    md={4}
                    lg={3}
                    key={index.toString()}
                  >
                    <CharacterListItem
                      id={character.id}
                      image={character.image}
                      name={character.name}
                      species={character.species}
                    />
                  </Grid>
                ))}
              </Grid>
              <Grid
                item
                xs={12}
                className='pagination'
              >
                <Pagination
                  count={characterList.data.characters.info.pages}
                  defaultPage={page}
                  siblingCount={0}
                  onChange={handleChangePage}
                />
              </Grid>
            </>
          )}
        </>
      )}
    </Grid>
  );
}
