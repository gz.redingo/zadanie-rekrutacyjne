import { createAction } from 'redux-actions';

import ACTION from '@src/redux/action-types';
import { api } from '@src/repository/api';

export const getCharacterList = createAction(ACTION.CHARACTER.LIST, (text = '', page = 1) => api({
  data: {
    query: `query characters {
      characters(page: ${ page }, filter: { name: "${ text }" }) {
        info {
          count
          pages
        }
        results {
          id,
          name,
          image,
          species
        }
      }
    }`,
  },
}));

export const getCharacterListEnd = createAction(ACTION.CHARACTER.LIST_END);
