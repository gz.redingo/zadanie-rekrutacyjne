import React, { Suspense } from 'react';
import { BrowserRouter as Router } from 'react-router-dom';
import { Loader } from '@components/shared';

import routes from '@src/router/routes';

const loading = () => (
  <Loader />
);

function App() {
  return (
    <Router>
      <Suspense fallback={loading()}>
        {routes}
      </Suspense>
    </Router>
  );
}

export default App;
