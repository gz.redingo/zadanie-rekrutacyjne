import React from 'react';
import PropTypes from 'prop-types';
import AppBar from '@material-ui/core/AppBar';
import Toolbar from '@material-ui/core/Toolbar';
import Typography from '@material-ui/core/Typography';
import Container from '@material-ui/core/Container';

export function Basic(props) {
  const { children, title } = props;
  return (
    <>
      <AppBar className='appBar' position='fixed'>
        <Toolbar>
          <Typography variant='h6'>
            {title}
          </Typography>
        </Toolbar>
      </AppBar>
      <Container className='main-layout'>
        {children}
      </Container>
    </>
  );
}

Basic.propTypes = {
  children: PropTypes.node.isRequired,
  title: PropTypes.string.isRequired,
};
