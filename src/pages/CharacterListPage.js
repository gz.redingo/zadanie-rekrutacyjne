import React, { useEffect } from 'react';
import { useDispatch } from 'react-redux';
import { CharacterList } from '@components/characterList';
import { getCharacterListEnd } from '@components/characterList/actions';

export default function CharacterListPage() {
  const dispatch = useDispatch();
  useEffect(() => () => {
    dispatch(getCharacterListEnd());
  });
  return <CharacterList />;
}
