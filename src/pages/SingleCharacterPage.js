import React, { useEffect } from 'react';
import { useDispatch } from 'react-redux';
import { SingleCharacter } from '@components/singleCharacter';
import { getSingleCharacterEnd } from '@components/singleCharacter/actions';

export default function CharacterListPage() {
  const dispatch = useDispatch();
  useEffect(() => () => {
    dispatch(getSingleCharacterEnd());
  });
  return <SingleCharacter />;
}
