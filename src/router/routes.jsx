import React from 'react';
import { Switch } from 'react-router-dom';

import { Basic } from '@layouts/Basic';
import AppRoute from './app-router';

const CharacterListPage = React.lazy(() => import('@pages/CharacterListPage'));
const SingleCharacterPage = React.lazy(() => import('@pages/SingleCharacterPage'));

const routes = (
  <Switch>
    <AppRoute
      exact
      path='/'
      layout={Basic}
      component={CharacterListPage}
      title='Rick and Morty'
    />
    <AppRoute
      exact
      path='/character/:id'
      layout={Basic}
      component={SingleCharacterPage}
      title='Rick and Morty'
    />
  </Switch>
);

export default routes;
