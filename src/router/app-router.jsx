import React from 'react';
import { Route } from 'react-router-dom';
import propTypes from 'prop-types';

function AppRouter(props) {
  const {
    component: Scene,
    layout: Layout,
    title,
    ...routeProps
  } = props;

  return (
    <Route
      {...routeProps}
      render={() => (
        <Layout title={title}>
          <Scene />
        </Layout>
      )}
    />
  );
}

AppRouter.defaultProps = {
  layout: false,
};

AppRouter.propTypes = {
  component: propTypes.any.isRequired,
  title: propTypes.string.isRequired,
  layout: propTypes.any,
};

export default AppRouter;
