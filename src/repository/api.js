import axios from 'axios';

const axiosConfig = {
  // eslint-disable-next-line no-underscore-dangle
  baseURL: window.config.API_URL,
  timeout: 20 * 1000,
  method: 'post',
};

export const api = axios.create(axiosConfig);
